using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class click : MonoBehaviour
{
    Ray fromCamera;
    RaycastHit hit;
    [SerializeField] GameObject towerPrefab;

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            fromCamera = GetComponent<Camera>().ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(fromCamera, out hit)) {
                Debug.Log("hit " + hit.transform.gameObject.tag);
                //if the hit tag == the placeable object tag then
                //spawn tower on hit pos

                //if(hit.transform.gameObject.tag == "placeable"){
                //  Instantiate(towerPrefab, hit.transform.position);  
                //}
            }
        }  
    }

    public void PlaceTower(Transform pos) {
        Instantiate(towerPrefab, pos);
    }
}
