using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour{
    [SerializeField] GameObject player;

    private void Update() {
        //Vector3 dir = player.transform.position - transform.position;
        //dir = new Vector3(dir.x, dir.y, 0);

        //gameObject.transform.rotation = Quaternion.Euler(dir);
        gameObject.transform.LookAt(player.transform);
        //gameObject.transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z);
    }
}
