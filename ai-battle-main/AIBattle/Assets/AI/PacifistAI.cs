﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//If a player reaches goal on same frame as bomb explode they will die, if they die there will the goal dissapear?
/*      Clear = 1, 
        Wall = 2,
        Enemy = 4,
        Bomb = 8,
        Diamond = 16,
        Goal = 32,
        OffGrid = 64
        May be Flag = 128
        May be Diamond = 256
 */

[CreateAssetMenu(fileName = "PacifistAI", menuName = "AI/Pacifist AI")]
public class PacifistAI : BaseAI {
    // Data here!
    int currentTurn = 0;
    Dictionary<Tuple<int, int>, byte> memorizedMap = new Dictionary<Tuple<int, int>, byte>();
    Dictionary<Tuple<int, int>, int> bombLocations = new Dictionary<Tuple<int, int>, int>();
    Tuple<int, int> playerPos = Tuple.Create(0, 0);
    Tuple<int, int, bool, bool> foundCrystal = Tuple.Create(0, 0, false, false); //crystal x, crystal y, found crystal, has crystal
    Tuple<int, int, bool> foundFlag = Tuple.Create(0, 0, false);
    bool moveFree = false;
    bool waitForMessage = false;
    bool cantWait = false;


    public override CombatantAction GetAction(ref List<GameManager.Direction> aMoves, ref int aBombTime) {
        Tuple<int, int> currentPos = Tuple.Create(playerPos.Item1, playerPos.Item2);

        //if bombs exist, rotate how they feel about them
        RotateBombs();

        //learn the map around themselves
        LearnTheMap(ref currentPos);

        //reset current position
        currentPos = Tuple.Create(playerPos.Item1, playerPos.Item2);

        //If found crystal pick it up only on an even number turn
        //If found flag and have crystal, go to flag only on odd number turn

        if (foundCrystal.Item3) { //found crystal
            if (foundCrystal.Item4) { //has crystal
                if (foundFlag.Item3) { //know where flag is
                    //go to flag only on odd number turn
                    if (currentTurn % 2 != 0) {
                        return PathTo(ref aMoves, ref aBombTime, Tuple.Create(foundFlag.Item1, foundFlag.Item2), ref currentPos);
                    }
                    else {
                        //make sure not next to a bomb
                    }
                }
                else {// don't know where flag is
                    ContinueSearch(ref aMoves, ref aBombTime);
                }
            }
            else { // don't have crystal
                //pick it up only on an even number turn
                if (currentTurn % 2 == 0)
                    return PathTo(ref aMoves, ref aBombTime, Tuple.Create(foundCrystal.Item1, foundCrystal.Item2), ref currentPos);
            }
        }
        else { //haven't found crystal
            //search while avoiding bombs
            return ContinueSearch(ref aMoves, ref aBombTime);
        }




        currentTurn++;
        return CombatantAction.Pass;
    }

    void Explore() {
        if (!checkedEntireMap()) {
           // Tuple <Tuple<byte, byte, byte, byte>, int> j = CheckAdjacentTilesToPos(playerPos.Item1, playerPos.Item2);
           /// if (j.Item2){
               
            //}
          //  List<Tuple<int, int>> clearAdjacentToUnchecked = FindAdjacentToUnchecked(); 
        }
    }

    List<Tuple<int,int>> FindAdjacentToUnchecked() {
        return new List<Tuple<int, int>>();
    }

    Tuple<Tuple<byte, byte, byte, byte>, int> CheckAdjacentTilesToPos(int x,int y) {
        Tuple<byte, byte, byte, byte> directions = Tuple.Create((byte)0,(byte)0,(byte)0,(byte)0);
        byte one, two, three, four;
        int numberOf = 0;
        one = 0;
        two = 0;
        three = 0;
        four = 0;
        if(memorizedMap.ContainsKey(Tuple.Create(x - 1, y))){
            one = memorizedMap[Tuple.Create(x - 1, y)];
            numberOf++;
        }
        if (memorizedMap.ContainsKey(Tuple.Create(x , y+1))) {
            two = memorizedMap[Tuple.Create(x, y+1)];
            numberOf++;
        }
        if (memorizedMap.ContainsKey(Tuple.Create(x + 1, y))) {
            three = memorizedMap[Tuple.Create(x + 1, y)];
            numberOf++;
        }
        if (memorizedMap.ContainsKey(Tuple.Create(x, y - 1))) {
            four = memorizedMap[Tuple.Create(x, y - 1)];
            numberOf++;
        }
        return Tuple.Create(Tuple.Create(one, two, three, four), numberOf);
    }

    CombatantAction PathTo(ref List<GameManager.Direction> aMoves, ref int aBombTime, Tuple<int, int> toPos, ref Tuple<int,int> currentPos) {
        //pathfind from playerPos to toPos and add each move to aMoves.
        if (moveFree) { // Don't need to move through clear spaces

        }
        else { // NEED to move through clear spaces

        }
        currentTurn++;
        return CombatantAction.Move;
    }

    CombatantAction ContinueSearch(ref List<GameManager.Direction> aMoves, ref int aBombTime) {
        if (checkedEntireMap()) { //if I've already checked the entire map
            //reset map knowledge except for known walls and offgrid and flag/crystal if known
            moveFree = true; //can now search with reckless abandon
            ResetRelativeMapKnowledge();
        }

        if (moveFree) {//being to move in knight-moves to non-wall non-offgrid positions

        }
        else {

        }
        return CombatantAction.Pass;
    }

    private void ResetRelativeMapKnowledge() { //sets all know "clear" spaces to unknown
        foreach(KeyValuePair<Tuple<int,int>,byte> pos in memorizedMap) {
            if(memorizedMap[pos.Key] == 1) {
                memorizedMap[pos.Key] = 0;
            }
        }
    }

    private bool checkedEntireMap() {
        return false;
    }

    private void LearnTheMap(ref Tuple<int, int> currentPos) {
        for (int i = 0; i < 5; i++) {
            switch (i) {
                case 0:
                    if (!memorizedMap.ContainsKey(currentPos))
                        memorizedMap.Add(currentPos, (byte)(UseSensor(GameManager.Direction.Current)));
                    else
                        memorizedMap[currentPos] = (byte)(UseSensor(GameManager.Direction.Current));
                    if ((memorizedMap[currentPos] & 8) == 1) { //have to have knight moved onto bomb, or is own bomb?
                        bombLocations.Add(currentPos, SenseBombDuration(GameManager.Direction.Current));
                    }
                    if ((memorizedMap[currentPos] & 16) == 1) { //if it has diamond
                        foundCrystal = Tuple.Create(currentPos.Item1, currentPos.Item2, true, true); //is ontop of diamond, so has diamond
                    }
                    if ((memorizedMap[currentPos] & 32) == 1) { //if it is flag
                        foundFlag = Tuple.Create(currentPos.Item1, currentPos.Item2, true);
                    }
                    break;
                case 1:
                    currentPos = Tuple.Create(playerPos.Item1, playerPos.Item2 + 1);
                    if (!memorizedMap.ContainsKey(currentPos))
                        memorizedMap.Add(currentPos, (byte)(UseSensor(GameManager.Direction.Up)));
                    else
                        memorizedMap[currentPos] = (byte)(UseSensor(GameManager.Direction.Up));
                    CheckForBomb(currentPos, GameManager.Direction.Up);
                    CheckForAlly(currentPos, GameManager.Direction.Up);
                    CheckForDiamond(currentPos);
                    CheckForFlag(currentPos);
                    break;
                case 2:
                    currentPos = Tuple.Create(playerPos.Item1, playerPos.Item2 - 1);
                    if (!memorizedMap.ContainsKey(currentPos))
                        memorizedMap.Add(currentPos, (byte)(UseSensor(GameManager.Direction.Down)));
                    else
                        memorizedMap[currentPos] = (byte)(UseSensor(GameManager.Direction.Down));
                    CheckForBomb(currentPos, GameManager.Direction.Down);
                    CheckForAlly(currentPos, GameManager.Direction.Down);
                    CheckForDiamond(currentPos);
                    CheckForFlag(currentPos);
                    break;
                case 3:
                    currentPos = Tuple.Create(playerPos.Item1 - 1, playerPos.Item2);
                    if (!memorizedMap.ContainsKey(currentPos))
                        memorizedMap.Add(currentPos, (byte)(UseSensor(GameManager.Direction.Left)));
                    else
                        memorizedMap[currentPos] = (byte)(UseSensor(GameManager.Direction.Left));
                    CheckForBomb(currentPos, GameManager.Direction.Left);
                    CheckForAlly(currentPos, GameManager.Direction.Left);
                    CheckForDiamond(currentPos);
                    CheckForFlag(currentPos);
                    break;
                case 4:
                    currentPos = Tuple.Create(playerPos.Item1 + 1, playerPos.Item2);
                    if (!memorizedMap.ContainsKey(currentPos))
                        memorizedMap.Add(currentPos, (byte)(UseSensor(GameManager.Direction.Right)));
                    else
                        memorizedMap[currentPos] = (byte)(UseSensor(GameManager.Direction.Right));
                    CheckForBomb(currentPos, GameManager.Direction.Right);
                    CheckForAlly(currentPos, GameManager.Direction.Right);
                    CheckForDiamond(currentPos);
                    CheckForFlag(currentPos);
                    break;
            }
        }
    }

    private void CheckForBomb(Tuple<int, int> currentPos, GameManager.Direction dir) {
        if ((memorizedMap[currentPos] & 8) == 1) { //if that position is a bomb
            bombLocations.Add(currentPos, SenseBombDuration(dir));
            if (bombLocations[currentPos] == 2) { //if the counter of the bomb is 2
                if ((memorizedMap[currentPos] & 4) == 1) { //if there is an enemy there
                                                           //if get name is ally, know flag is adjacent to bomb
                    if (SenseEnemyName(dir) == "!Agr0*") {
                        //depending on current turn number, know flag or crystal is adjacent
                        if (currentTurn % 2 == 0) {
                            GuessDiamond(currentPos, true);
                            //guess where diamond is
                        }
                        else {
                            GuessFlag(currentPos, true);
                            //guess where flag is
                        }
                    }
                    else {
                        waitForMessage = false;
                        cantWait = true;
                    }
                }
                else {
                    waitForMessage = false;
                    cantWait = true;
                }
                
            }
        }
    }

    public void CheckForAlly(Tuple<int, int> currentPos, GameManager.Direction dir) {
        if ((memorizedMap[currentPos] & 4) == 1) {
            //if get name is ally, know flag or diamond is adjacent, and expect bomb soon. Gauge based on current turn

        }
    }

    public void CheckForFlag(Tuple<int,int> currentPos) {
        if ((memorizedMap[currentPos] & 32) == 1) { //if it is flag
            foundFlag = Tuple.Create(currentPos.Item1, currentPos.Item2, true);
        }
    }

    public void CheckForDiamond(Tuple<int, int> currentPos) {
        if ((memorizedMap[currentPos] & 16) == 1) { //if it has diamond
            if ((memorizedMap[currentPos] & 4) == 1) { // enemy has diamond
                //idk what to do abt that tbh... rip
            }
            else {
                foundCrystal = Tuple.Create(currentPos.Item1, currentPos.Item2, true, false);
            }
        }
    }

    private void GuessFlag(Tuple<int, int> currentPos ,bool hasBomb) {
        //"current position" is !Agr0*
        if (hasBomb) {
            for (int i = 0; i < 4; i++) {
                switch (i) {
                    case 0:
                        currentPos = Tuple.Create(currentPos.Item1, currentPos.Item2 + 1);
                        break;
                    case 1:
                        currentPos = Tuple.Create(currentPos.Item1, currentPos.Item2 - 2);
                        break;
                    case 2:
                        currentPos = Tuple.Create(currentPos.Item1 - 1, currentPos.Item2 + 1);
                        break;
                    case 3:
                        currentPos = Tuple.Create(currentPos.Item1 + 2, currentPos.Item2);
                        break;
                }

                if (!memorizedMap.ContainsKey(currentPos)) {
                    memorizedMap.Add(currentPos, 128); //might be flag
                }
            }
            currentPos = Tuple.Create(currentPos.Item1 - 1, currentPos.Item2);
        }
        else { //be prepared to recieve a message, assuming I won't die standing here
            if (!cantWait)
                waitForMessage = true;
        }

    }

    private void GuessDiamond(Tuple<int, int> currentPos ,bool hasBomb) {
        //"current position" is !Agr0*

    }

    private void RotateBombs() {
        Dictionary<Tuple<int, int>, int> bombsToRemove = new Dictionary<Tuple<int, int>, int>();
        foreach (KeyValuePair<Tuple<int, int>, int> entry in bombLocations) {
            SubBombs(entry.Key);
            if (entry.Value <= 1) {
                memorizedMap[entry.Key] = 1;
                bombsToRemove.Add(entry.Key, entry.Value);
            }
        }
        foreach(KeyValuePair<Tuple<int,int>,int> entry in bombsToRemove) {
            if (bombLocations.ContainsKey(entry.Key)) {
                bombLocations.Remove(entry.Key);
            }
        }
    }

    private void SubBombs(Tuple<int,int> a) {
        bombLocations[a]--;
    }
}
