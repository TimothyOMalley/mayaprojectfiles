using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FlagGuardianAI", menuName = "AI/Flag Guardian AI")]
public class FlagGuardianAI : BaseAI {
    // Data here!
    int turn = 0;
    //message format
    //8-0000 7-0000  6-0000 5-0000  4-0000 3-0000  2-0000 1-0000
    //1- msg, found, crystal, flag
    //2- up, down, left, right
    //3- up, down, left, right
    //4- up, down, left, right
    //5- up, down, left, right
    //6- up, down, left, right
    //7- up, down, left, right
    //8- up, down, left, right,
    //if left & right are true, end of message
    //if up and right, but it needs to go up first, "right" will be 0 as to not lead pacifist into wall

    public override CombatantAction GetAction(ref List<GameManager.Direction> aMoves, ref int aBombTime) {
        return CombatantAction.Pass;
    }
}
